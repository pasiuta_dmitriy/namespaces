<?php
namespace App;
include __DIR__ . "/vendor/autoload.php";

$objects = [
    new \App\Objects\MyClass(),
    new \NamespaceProject\Map\Sea(),
    new \NamespaceProject\Map\Mountain(),
    new \NamespaceProject\Unit\Sea\UBoat(),
    new \NamespaceProject\Unit\Flying\UFO(),
];

foreach ($objects as $object) {
    echo get_class($object) . "; \n";
}
<?php

declare(strict_types=1);


namespace QI\SimpleMvcBlog\View;


interface ViewInterface
{
    public function render(): string;
}
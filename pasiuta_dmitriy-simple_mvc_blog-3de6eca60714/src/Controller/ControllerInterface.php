<?php

declare(strict_types=1);


namespace QI\SimpleMvcBlog\Controller;


use QI\SimpleMvcBlog\View\ViewInterface;

interface ControllerInterface
{
    public function index(): ViewInterface;
}
<?php
namespace QI\SimpleMvcBlog\Controller;
use QI\SimpleMvcBlog\Storage\HardCodePost;
use QI\SimpleMvcBlog\View\Posts;
use QI\SimpleMvcBlog\View\ViewInterface;
use QI\SimpleMvcBlog\Model;
class Post implements ControllerInterface{
    private $model;
    public function __construct(){
        $this->model=new HardCodePost();
    }
    public function add($posts){
        foreach ($posts as $blogPost){
        $blogPost=new Model\Post($posts);
        $newPosts=[];
        array_push($newPosts,$blogPost);
        $this->model->add($blogPost);
        }
        return new Posts('Post was add.',$newPosts);

    }

    public function show($postsId){
        foreach ($postsId as $postId){
            $posts[]=$this->model->search($postId);
        }
        return new Posts("Posts:",$posts);
    }

    public function index(): ViewInterface
    {
        return new Posts('Posts',$this->model->all());
    }
}